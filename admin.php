<?php
header("content-type:text/html;charset=utf-8");
error_reporting(0);
define('BIND_MODULE', 'Admin');
define('APP_DEBUG', true);	//开启调试模式
if(file_exists("./install") && !file_exists("./install/install.lock")){
    // 组装安装url
    $url=$_SERVER['HTTP_HOST'].trim($_SERVER['SCRIPT_NAME'],'index.php').'install/index.php';
    // 使用http://域名方式访问；避免./Public/install 路径方式的兼容性和其他出错问题
    header("Location:http://$url");
    die;
}
include "./App/ThinkPHP.php";//引入ThinkPHP核心文件
