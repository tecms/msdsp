<?php
namespace Home\Controller;

use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {
    	$info=M('info')->where("id=1")->find();
        $notice=M('notice')->where("id=1")->find();
        $this->assign('notice',$notice);
        $istype=(int)$info['istype'];
        if ($istype==1) {
        	$list=M('interface')->select();
        	$this->assign('list',$list);
        }else{
        	$list=$this->gettype();
			$listarr = json_decode($list,true);
			$this->assign('list',$listarr);
        }
    	$this->info();
        $this->display();
    }
    //远程获取
  	function gettype(){
  		$url="https://api.tecms.net/api/type.php";
    	$curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
	    $data = curl_exec($curl);
		curl_close($curl);
		return $data;
  	}

    public function dsp()
    {
    	$this->info();
    	$this->display();
    }
    public function downv()
    {
        header( "Content-Type: video/mp4" );  
        header("Content-Disposition: attachment;filename=".date('Y-m-d H:i:s', time())."下载.mp4");
        $movie = file_get_contents($_GET['url']);
        echo $movie;
    }
    public function jiexi()
    {	
    	if(!IS_AJAX){
    		$this->error('提交方式错误!');
    	}else{
    		$info=M('info')->where("id=1")->find();
    		$islogin=(int)$info['islogin'];
    		$isvip=(int)$info['isvip'];

    		$interface=$info['dspurl'];
    		//判断是否开启登录使用
    		if ($islogin==1) {
    			$s=(int)$_SESSION['user']['id'];
		    	if (empty($s)) {
		    		$this->error("请登录后使用");
		    	}else{

		    		//已登录判断是否是vip才能使用
		    		if ($isvip==1) {
                        $re=M('user')->where("id=$s")->find();
                        $uservip=(int)$re['isvip'];//0为会员 //1不是
                        if ($uservip==0) {
                            //查询限制次数
                            $web=M('info')->where("id=1")->find();
                            $vcount=(int)$web['vipcount'];
                            if ($vcount==0) {
                                $this->qsy(remove_xss(trim($_POST['url'])));
                            }
                            //VIP剩余次数
                            $userinfo=M('user')->where("id=$s")->find();
                            $vipcount=(int)$userinfo['vipcount'];
                            if ($vipcount>0) {
                                $v_data['vipcount']=$vipcount-1;
                                $re=M('user')->where("id=$s")->save($v_data);
                                $this->qsy(remove_xss(trim($_POST['url'])));
                                
                            }else{
                                $this->error("剩余次数不足，请联系站长");
                            }
                            $this->qsy(remove_xss(trim($_POST['url'])));
                        }
		    			$this->error("请先开通VIP后使用");
		    		}elseif($isvip==0){
                        
		    			$user=M('user')->where("id=$s")->find();
						$status=(int)$user['status'];
						if ($status==1) {
							$this->error("该帐号已被禁止使用本站功能");
						}else{
                            //普通用户次数限制
                            //不限制使用
                            $web=M('info')->where("id=1")->find();
                            $ucount=(int)$web['usercount'];
                            if ($ucount==0) {
                                $this->qsy(remove_xss(trim($_POST['url'])));
                            }
                            //限制用户次数
                            $userinfo=M('user')->where("id=$s")->find();
                            $usercount=(int)$userinfo['usercount'];
                            if ($usercount>0) {
                                $u_data['usercount']=$usercount-1;
                                $re=M('user')->where("id=$s")->save($u_data);
                                $this->qsy(remove_xss(trim($_POST['url'])));

                            }else{
                                $this->error("剩余次数不足，请尝试开通VIP");
                            }
							
						}
		    		}
		    	}
    		}
    		//如果关闭登录使用
    		if ($islogin==0) {
    			$this->qsy(remove_xss(trim($_POST['url'])));
    		}

		}
    }
    function info(){
      $info=M('info')->where("id=1")->find();
      $this->assign('info',$info);
    }
    function qsy($url){
    	$url=remove_xss(trim($url));
		$str_r= '/(http:\/\/|https:\/\/)((\w|=|\?|\.|\/|&|-)+)/';
		preg_match_all($str_r,$url,$arr);
		$url=$arr[0][0];
		$info=M('info')->where("id=1")->find();
		$interface=$info['dspurl'];
		$url=$interface.$url;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
		$data = curl_exec($curl);
		curl_close($curl);
		$arrdata = json_decode($data,true);
		$this->ajaxReturn(array(
			"info"=>$arrdata['msg'],
			"url"=>$arrdata['data']['url'],
		));
			
    }
    public function testapi(){
        $data='';
        $arrdata = json_decode($data,true);
        var_dump($arrdata);
    }
}
