<?php
namespace Admin\Controller;
use Think\Controller;
class UserController extends Controller {
	//判断是否登录
	public function _initialize(){
	    if ($_SESSION['admin']['id']=="") {
           $this->redirect('login/index');
        }
	}
	public function index(){
		$list=M('user')->select();
		// foreach ($list as $k => $v) {
  //           # code...
  //           $list[$k]['id']=M('vip')->where("uid=".$v['id'])->find();
  //       }
  //       var_dump($list);
		$this->assign('list',$list);
		$this->display();
	}
	public function userdel(){
		if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $re=M('user')->where("id=$id")->delete();
            $res=M('vip')->where("uid=$id")->delete();
            if (!empty($re)) {
                $this->success("删除成功！",U('user/index'));
            }else{
                $this->error("删除失败");
            }
        }
	}
	//禁用
	public function userjy(){
		if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $data['status']=1;
            $re=M('user')->where("id=$id")->save($data);
            if (!empty($re)) {
                $this->success("状态修改成功",U('user/index'));
            }else{
                $this->error("状态修改失败");
            }
        }
	}
	//启用
	public function userqy(){
		if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $data['status']=0;
            $re=M('user')->where("id=$id")->save($data);
            if (!empty($re)) {
                $this->success("状态修改成功",U('user/index'));
            }else{
                $this->error("状态修改失败");
            }
        }
	}
	//重置密码
    public function usercpass(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
        	$id=(int)remove_xss(I('post.id','','strip_tags'));
            $data['password']=md5(123456);
            $re=M('user')->where("id=$id")->save($data);
            if (!empty($re)) {
                $this->success("重置成功！",U('user/index'));
            }else{
                $this->error("该用户密码已是默认密码");
            }
        }
    }
    public function addkami(){
    	if (!IS_AJAX) {
    		$this->display();
    	}else{
    		$count=(int)remove_xss(I('post.count','','strip_tags'));
    		$type=(int)remove_xss(I('post.sta','','strip_tags'));
    		//生成月卡
    		if ($type==1) {
    			for ($x=0; $x<$count; $x++) {
  					$data['viptext']="M".randomkeys(6).generateRandomString();
  					$data['type']=1;
  					$re=M('vipkami')->add($data);
  					
				}
				if (!empty($re)) {
  					$this->success("生成成功！",U('user/kmlist'));
  				}else{
  					$this->error("生成失败！");
  				}
    		}
    		//生成年卡
    		if ($type==2) {
    			for ($x=0; $x<$count; $x++) {
  					$data['viptext']="Y".randomkeys(6).generateRandomString();
  					$data['type']=2;
  					$re=M('vipkami')->add($data);
  					
				} 
				if (!empty($re)) {
  					$this->success("生成成功！",U('user/kmlist'));
  				}else{
  					$this->error("生成失败！");
  				}
    		}
    	}
    	
    }
    public function kmlist(){
    	$list=M('vipkami')->select();
    	$this->assign('list',$list);
    	$this->display();
    }
    //删除已用卡密
    public function kmdelall(){
    	if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
        	$id=(int)remove_xss(I('post.id','','strip_tags'));
           	if ($id==3) {
           		# code...
           		
        		$re=M('vipkami')->where("status=1")->delete();

        		if(!empty($re)){
        			$this->success('删除成功！',U('user/kmlist'));
        		}else{
        			$this->error('删除失败！');
        		}
           	}else{
           		$this->error('参数错误');
           	}
        }
    }
    public function txt(){
        $type=(int)remove_xss(I('get.type','','strip_tags'));
        //导出所有
        if ($type==1) {
        	$goods_list = M('vipkami')->order("id")->select();
        }
        //导出所有月卡
        if ($type==11) {
        	$goods_list = M('vipkami')->where("type=1")->order("id")->select();
        }
        //导出所有月卡
        if ($type==12) {
        	$goods_list = M('vipkami')->where("type=2")->order("id")->select();
        }
        //导出所有未使用月卡
        if ($type==21) {
        	$goods_list = M('vipkami')->where("type=1 AND status=0")->order("id")->select();
        }
        //导出所有未使用年卡
        if ($type==22) {
        	$goods_list = M('vipkami')->where("type=2 AND status=0")->order("id")->select();
        }
        foreach ($goods_list as $value){    
            $value = join("----",$value);   
            $temp[] = $value;    

        }    
        foreach($temp as $v){    
            $str.=$v."\r\n";    
        }    
        //    $str = substr($t,0,-1);  //利用字符串截取函数消除最后一个逗号    
        //    return $str;
        //dump($str);die;


        //执行
        $id=array($str);
        header("Content-type:application/octet-stream");
        header("Accept-Ranges:bytes");
        header("Content-Disposition:attachment;filename=".'卡密列表_'.date("YmdHis").".txt");
        header("Expires: 0");
        header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
        header("Pragma:public");
        echo implode(",",$id);
       
    }
    public function kmdel(){
        if (!IS_AJAX) {
            $this->error("非法请求");
        }else{
            $id=(int)remove_xss(I('post.id','','strip_tags'));
            $re=M('vipkami')->where("id=$id")->delete();
            if (!empty($re)) {
                $this->success("删除成功！",U('user/kmlist'));
            }else{
                $this->error("删除失败");
            }
        }
    }
}