<?php
namespace Admin\Controller;
use Think\Controller;
class SystemController extends Controller {
	//判断是否登录
	public function _initialize(){
	    if ($_SESSION['admin']['id']=="") {
           $this->redirect('login/index');
        }
	}
	public function index(){
		if (!IS_AJAX) {
			$id=(int)$_SESSION['admin']['id'];
			$re=M('admin')->where("id=$id")->find();
			$this->assign('info',$re);
			$this->display();
		}else{
			$id=(int)$_SESSION['admin']['id'];
			$pass=remove_xss(I('post.pass','','strip_tags'));
			if (empty($pass)) {
				$data['username']=remove_xss(I('post.name','','strip_tags'));
				$data['nickname']=remove_xss(I('post.nname','','strip_tags'));
				$data['qq']=remove_xss(I('post.qq','','strip_tags'));
				
			}else{
				$data['username']=remove_xss(I('post.name','','strip_tags'));
				$data['nickname']=remove_xss(I('post.nname','','strip_tags'));
				$data['qq']=remove_xss(I('post.qq','','strip_tags'));
				$data['password']=remove_xss(jmppwd(I('post.pass','','strip_tags')));
			}
			$res=M('admin')->where("id=$id")->save($data);
			if (!empty($res)) {
				$this->success('修改成功',U('system/index'));
			}else{
				$this->success('修改失败');
			}
		}
		
	}
	public function webinfo(){
		if (!IS_AJAX) {
			$re=M('info')->where("id=1")->find();
			$this->assign('info',$re);
			$this->display();
		}else{
			$data['logo']=remove_xss(I('post.logo','','strip_tags'));
			$data['title']=remove_xss(I('post.title','','strip_tags'));
			$data['banquan']=remove_xss(I('post.bq','','strip_tags'));
			$data['banquan1']=$_POST['bq1'];
			$data['islogin']=remove_xss(I('post.login','','strip_tags'));
			$data['isvip']=remove_xss(I('post.vip','','strip_tags'));
			$data['dspurl']=remove_xss(I('post.url','','strip_tags'));
			$data['tcode']=$_POST['tcode'];
			$data['usercount']=(int)$_POST['ucount'];
			$data['vipcount']=(int)$_POST['vcount'];
			
			$res=M('info')->where("id=1")->save($data);

			if (!empty($res)) {
				$this->success('修改成功',U('system/webinfo'));
			}else{
				$this->error('修改失败');
			}
		}
	}
	public function notice(){
		if (!IS_AJAX) {
			$notice=M('notice')->where("id=1")->find();
			$this->assign('notice',$notice);
			$this->display();
		}else{
			$notice_data['title']=remove_xss($_POST['title']);
			$notice_data['notice']=$_POST['message'];
			$notice_data['addtime']=time();
			$notice_data['status']=(int)$_POST['sta'];
			$re=M('notice')->where("id=1")->save($notice_data);
			if (!empty($re)) {
				$this->success("设置成功");
			}else{
				$this->error("设置失败");
			}
		}
	}
}