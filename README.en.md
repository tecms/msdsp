# 默笙短视频去水印

#### 安装教程

1. 访问站点执行安装程序
2. 后台账号密码：admin/123456
3. 安装完成后请删除/install文件夹

#### 更新说明
<li>2019年3月4日<img src="https://www.tecms.net/tip.png" width="2%" height="2%" alt="提示">修复前台点击logo跳转问题，新增点击下载视频按钮下载直接视频功能</li>


#### 版本说明

1. V1.0会员系统，前后台自适应
2. V1.0后台登录验证码校验
3. V1.0双重xss验证，防止xss注入
4. V1.0后台密码双重加密，保护数据安全
5. V1.0后台可设置关闭或开启远程获取支持解析
6. V1.0后台可设置是否免登录和VIP使用
7. V1.0后台可一建生成卡密支持导出.txt文档

#### 售后群

1. QQ群 [484395502](https://jq.qq.com/?_wv=1027&k=5cjRSNM)

#### 赞助
<img src="https://www.tecms.net/zz_zfb.jpg" width="30%" height="30%" alt="支付宝">
<img src="https://www.tecms.net/zz_wx.jpg" width="30%" height="30%" alt="微信">

#### 赞助列表
<table>
    <tr>
        <td>姓名</td>
        <td>金额</td>
    </tr>
    <tr>
        <td>阁主</td>
        <td>6.66</td>
    </tr>
</table>
